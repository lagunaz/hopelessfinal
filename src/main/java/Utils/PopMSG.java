/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Code.Addict
 */
public class PopMSG {
	
	public static void warning(String header, String body){
		JOptionPane.showMessageDialog(new JFrame(), body, header, JOptionPane.WARNING_MESSAGE);
	}

	public static void plain(String header, String body){
		JOptionPane.showMessageDialog(new JFrame(), body, header, JOptionPane.PLAIN_MESSAGE);
	}

	public static void info(String header, String body){
		JOptionPane.showMessageDialog(new JFrame(), body, header, JOptionPane.INFORMATION_MESSAGE);
	}

	public static void error(String header, String body){
		JOptionPane.showMessageDialog(new JFrame(), body, header, JOptionPane.ERROR_MESSAGE);
	}

}
