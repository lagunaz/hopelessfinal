/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
/**
 *
 * @author norma
 */
public class User {
    private int id ;
    private String firstname;       
    private String lastname;
    private String username;
    private String userpassword;
    private String rank;
    private int    salary;
    private String tel;

    public User(String firstname, String lastname, String username, String userpassword, String rank, int salary, String tel) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.userpassword = userpassword;
        this.rank = rank;
        this.salary = salary;
        this.tel = tel;
    }
    
   
    
    public User(int id, String firstname, String lastname, String username, String userpassword, String rank, int salary, String tel) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.userpassword = userpassword;
        this.rank = rank;
        this.salary = salary;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", username=" + username + ", userpassword=" + userpassword + ", rank=" + rank + ", salary=" + salary + ", tel=" + tel + '}';
    }
    
    
            
            
}
