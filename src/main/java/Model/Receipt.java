/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author User
 */
public class Receipt {
    private int id;
    private Date time;
    private User user;
    private Customer cus;
    private ArrayList<ReceiptDetail> receiptDetail;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public Receipt(int id, Date time, User user, Customer cus) {
        this.id = id;
        this.time = time;
        this.user = user;
        this.cus = cus;
        this.receiptDetail = new ArrayList<>();
    }

    public Receipt(Date time, User user, Customer cus) {
        this.time = time;
        this.user = user;
        this.cus = cus;
        this.receiptDetail = new ArrayList<>();
    }
    

    
    
    public void addReceiptDetail(int id, Product prod, int amount, double price) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProd().getId() == prod.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(id, prod, amount, price, this));
    }
    
    public void addReceiptDetail(Product prod, int amount) {
        addReceiptDetail(-1, prod, amount, prod.getPrice());
    }
    
    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);
    }
    
    public double getTotal() {
        double total = 0;
        for (ReceiptDetail r : receiptDetail) {
            total = total + r.getTotal();
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCus() {
        return cus;
    }

    public void setCus(Customer cus) {
        this.cus = cus;
    }

    
    
    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }
    
    public String format(Date date){
        return dateFormat.format(date);
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id
                + ", Time=" + format(time)
                + ", Employee=" + user
                + ", Customer=" + cus
                + " total=" + this.getTotal()
                + "}\n";
        for (ReceiptDetail r : receiptDetail) {
            str = str + r.toString() + "\n";
        }
        return str;
    }
    
    
}
