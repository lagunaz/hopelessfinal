/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author norma
 */
public class ReceiptDetail {
    private int id;
    private Product prod;
    private int amount;
    private double price;
    private Receipt receipt;

    public ReceiptDetail(int id, Product prod, int amount, double price, Receipt receipt) {
        this.id = id;
        this.prod = prod;
        this.amount = amount;
        this.price = price;
        this.receipt = receipt;
    }

    public ReceiptDetail(Product prod, int amount, double price, Receipt receipt) {
        this.prod = prod;
        this.amount = amount;
        this.price = price;
        this.receipt = receipt;
    }
    
    public double getTotal() {
        return amount * price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProd() {
        return prod;
    }

    public void setProd(Product prod) {
        this.prod = prod;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }
    
    public void addAmount(int amount){
        this.amount = this.amount + amount;
    }
    
    @Override
    public String toString() {
        return "ReceipDetail{" + "id=" + id
                + ", product=" + prod
                + ", amount=" + amount
                + ", price=" + price
                + ", total=" + this.getTotal()
                + '}';
    }

    
    
}
