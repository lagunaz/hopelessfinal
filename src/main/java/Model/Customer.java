/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author norma
 */
public class Customer {
    private int id;
    private String firstname;
    private String lastname;
    private int point;
    private String tel;

    public Customer(int id, String firstname, String lastname, int point, String tel) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.point = point;
        this.tel = tel;
    }

    public Customer(String firstname, String lastname, int point, String tel) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.point = point;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", point=" + point + ", tel=" + tel + '}';
    }
    
    
    
    
}
