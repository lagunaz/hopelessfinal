/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import Model.Product;
import Model.Receipt;
import Model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    private UserDao userDao = new UserDao();
    private CustomerDao cusDao = new CustomerDao();
    private ProductDao prodDao = new ProductDao();

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt (receipt_time,Cus_id,User_id,rec_total) VALUES (?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateFormat.format(object.getTime()));
            stmt.setInt(2, object.getCus().getId());
            stmt.setInt(3, object.getUser().getId());
            stmt.setDouble(4, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO Receipt_Detail (Rec_price,Rec_amount,Rec_receiptID,Rec_productID) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setDouble(1, r.getPrice());
                stmtDetail.setInt(2, r.getAmount());
                stmtDetail.setInt(3, r.getReceipt().getId());
                stmtDetail.setInt(4, r.getProd().getId());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create receipt");
        }
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.receipt_id as id,\n"
                    + "       r.receipt_time as time,\n"
                    + "       r.Cus_id as cusId,\n"
                    + "       r.User_id as userId,\n"
                    + "       r.rec_total as total\n"
                    + " FROM Receipt r, Customer c, User u\n"
                    + " WHERE r.Cus_id = c.cus_id AND r.User_id = u.user_id"
                    + " ORDER BY r.receipt_time DESC;";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("id");
                Date created = (Date) dateFormat.parse(result.getString("time"));
                int customerId = result.getInt("cusId");
                int userId = result.getInt("userId");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        userDao.get(userId), cusDao.get(customerId));
                getReceiptDetail(conn, id, receipt);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) {
        try {
            String sqlDetail = "SELECT Rec_id,\n"
                    + "        Rec_price,\n"
                    + "        Rec_amount,\n"
                    + "        Rec_receiptID,\n"
                    + "        Rec_productID\n"
                    + "        FROM Receipt_Detail rd, Product p\n"
                    + "        WHERE Rec_receiptID = ? AND rd.Rec_productID = p.product_id;";
            PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
            stmtDetail.setInt(1, id);
            ResultSet resultDetail = stmtDetail.executeQuery();

            while (resultDetail.next()) {
                int receiveId = resultDetail.getInt("Rec_id");
//                int productId = resultDetail.getInt("Prose_id");
                double price = resultDetail.getDouble("Rec_price");
                int amount = resultDetail.getInt("Rec_amount");
                Product product = prodDao.get(resultDetail.getInt("Rec_productID"));
                receipt.addReceiptDetail(receiveId, product, amount, price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.receipt_id as id,\n"
                    + "       r.receipt_time as time,\n"
                    + "       r.Cus_id as cusId,\n"
                    + "       r.User_id as userId,\n"
                    + "       r.rec_total as total\n"
                    + " FROM Receipt r, Customer c, User u\n"
                    + " WHERE id = ? AND r.Cus_id = c.cus_id AND r.User_id = u.user_id"
                    + " ORDER BY r.receipt_time DESC;";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                int pid = result.getInt("id");
                Date created = (Date) dateFormat.parse(result.getString("time"));
                int customerId = result.getInt("cusId");
                int userId = result.getInt("userId");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        userDao.get(userId), cusDao.get(customerId));
                getReceiptDetail(conn, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt WHERE receipt_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect receipt id " + id + "!!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
