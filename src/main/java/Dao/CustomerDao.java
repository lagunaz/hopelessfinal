/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import Model.Customer;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author norma
 */
public class CustomerDao implements DaoInterface<Customer> {

	@Override
	public int add(Customer object) {
		Connection con = null;
		Database db = Database.getInstance();
		con = db.getConnection();
		int id = -1;
		try {
			String sql = "INSERT INTO Customer (cus_firstname,"
				+ "cus_lastname,cus_point,cus_tel )VALUES (?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, object.getFirstname());
			stmt.setString(2, object.getLastname());
			stmt.setString(3, "" + object.getPoint());
			stmt.setString(4, object.getTel());
			int row = stmt.executeUpdate();
			ResultSet result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			}
		} catch (SQLException ex) {
			Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return id;
	}

	@Override
	public ArrayList<Customer> getAll() {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT cus_id,cus_firstname,cus_lastname,cus_point,"
				+ "cus_tel FROM Customer;";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("cus_id");
				String fname = result.getString("cus_firstname");
				String lname = result.getString("cus_lastname");
				int point = result.getInt("cus_point");
				String tel = result.getString("cus_tel");

				Customer cus = new Customer(id, fname, lname, point, tel);
				list.add(cus);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return list;
	}

	@Override
	public Customer get(int id) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT cus_id,cus_firstname,cus_lastname,cus_point,"
				+ "cus_tel FROM Customer WHERE cus_id=" + id;
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int idDontuse = result.getInt("cus_id");
				String fname = result.getString("cus_firstname");
				String lname = result.getString("cus_lastname");
				int point = result.getInt("cus_point");
				String tel = result.getString("cus_tel");
				Customer user = new Customer(id, fname, lname, point, tel);
				return user;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return null;
	}

	@Override
	public int delete(int id) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "DELETE FROM Customer WHERE cus_id  = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;
	}

	@Override
	public int update(Customer object) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "UPDATE Customer SET cus_id = ?,cus_firstname = ?,"
				+ "cus_lastname = ?,cus_point = ?,cus_tel = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, object.getId());
			stmt.setString(2, object.getFirstname());
			stmt.setString(3, object.getLastname());
			stmt.setString(4, "" + object.getPoint());
			stmt.setString(5, object.getTel());

			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;
	}

	public Customer get(String tel) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT cus_id,cus_firstname,cus_lastname,cus_point"
				+ " FROM Customer WHERE cus_tel=\"" +tel+"\"";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("cus_id");
				String fname = result.getString("cus_firstname");
				String lname = result.getString("cus_lastname");
				int point = result.getInt("cus_point");
				Customer user = new Customer(id, fname, lname, point, tel);
				return user;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return null;
	}

}
