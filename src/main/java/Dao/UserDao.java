/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author norma
 */
public class UserDao implements DaoInterface<User> {

	@Override
	public int add(User object) {
		Connection con = null;
		Database db = Database.getInstance();
		con = db.getConnection();
		int id = -1;
		try {
			String sql = "INSERT INTO User (user_firstname,"
				+ "user_lastname,user_username,user_userpassword,user_rank,"
				+ "user_salary,user_tel )VALUES (?,?,?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, object.getFirstname());
			stmt.setString(2, object.getLastname());
			stmt.setString(3, object.getUsername());
			stmt.setString(4, object.getUserpassword());
			stmt.setString(5, object.getRank());
			stmt.setString(6, "" + object.getSalary());
			stmt.setString(7, object.getTel());
			int row = stmt.executeUpdate();
			ResultSet result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return id;
	}

	@Override
	public ArrayList<User> getAll() {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT user_id,user_firstname,user_lastname,user_username,"
				+ "user_userpassword,user_rank,user_salary,user_tel FROM User;";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("user_id");
				String fname = result.getString("user_firstname");
				String lname = result.getString("user_lastname");
				String username = result.getString("user_username");
				String password = result.getString("user_userpassword");
				String rank = result.getString("user_rank");
				int salary = result.getInt("user_salary");
				String tel = result.getString("user_tel");
				User user = new User(id, fname, lname, username, password, rank, salary, tel);
				list.add(user);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return list;
	}

	@Override
	public User get(int id) {

		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT user_id,user_firstname,user_lastname,user_username,"
				+ "user_userpassword,user_rank,user_salary,user_tel FROM User WHERE user_id=" + id;
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int idDontuse = result.getInt("user_id");
				String fname = result.getString("user_firstname");
				String lname = result.getString("user_lastname");
				String username = result.getString("user_username");
				String password = result.getString("user_userpassword");
				String rank = result.getString("user_rank");
				int salary = result.getInt("user_salary");
				String tel = result.getString("user_tel");
				User user = new User(id, fname, lname, username, password, rank, salary, tel);
				return user;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return null;
	}

	@Override
	public int delete(int id) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "DELETE FROM User WHERE user_id  = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;
	}

	@Override
	public int update(User object) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "UPDATE User SET user_id = ?,user_firstname = ?,"
				+ "user_lastname = ?,user_username = ?,user_userpassword = ?,"
				+ "user_rank = ?,user_salary = ?,user_tel = ?"
				+ "WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, object.getId());
			stmt.setString(2, object.getFirstname());
			stmt.setString(3, object.getLastname());
			stmt.setString(4, object.getUsername());
			stmt.setString(5, object.getUserpassword());
			stmt.setString(6, object.getRank());
			stmt.setString(7, "" + object.getSalary());
			stmt.setString(8, object.getTel());
			stmt.setInt(9, object.getId());
			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;
	}

	public User login(String username, String password) {

		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT user_id,user_firstname,user_lastname,user_username,"
				+ "user_userpassword,user_rank,user_salary,user_tel FROM User WHERE user_username=\"" + username + "\" AND user_userpassword = \"" + password + "\"";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("user_id");
				String fname = result.getString("user_firstname");
				String lname = result.getString("user_lastname");
				String username1 = result.getString("user_username");
				String password1 = result.getString("user_userpassword");
				String rank = result.getString("user_rank");
				int salary = result.getInt("user_salary");
				String tel = result.getString("user_tel");
				User user = new User(id, fname, lname, username, password, rank, salary, tel);
				return user;
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return null;
	}

}
