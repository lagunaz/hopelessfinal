/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.Database;
import Model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class ProductDao implements DaoInterface<Product> {

	@Override
	public int add(Product object) {
		Connection con = null;
		Database db = Database.getInstance();
		con = db.getConnection();
		int id = -1;
		try {
			String sql = "INSERT INTO Product (product_name, product_price, product_img) VALUES (?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, object.getName());
			stmt.setInt(2, object.getPrice());
			stmt.setString(3, object.getImg());
			stmt.executeUpdate();
			ResultSet result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			}
		} catch (SQLException ex) {
			Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return id;
	}

	@Override
	public ArrayList<Product> getAll() {
		ArrayList list = new ArrayList();
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT product_id,product_name,product_price," + "product_img FROM Product;";
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				int id = result.getInt("product_id");
				String fname = result.getString("product_name");
				int price = result.getInt("product_price");
				String img = result.getString("product_img");

				Product user = new Product(id, fname, price, img);
				list.add(user);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return list;
	}

	@Override
	public Product get(int id) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		try {
			String sql = "SELECT product_id,product_name,product_price,product_img"
				+ " FROM Product WHERE product_id = " + id;
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			while (result.next()) {
				String fname = result.getString("product_name");
				int price = result.getInt("product_price");
				String img = result.getString("product_img");
				Product user = new Product(id, fname, price, img);
				return user;
			}
		} catch (SQLException ex) {
			Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return null;
	}

	@Override
	public int delete(int id) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "DELETE FROM Product WHERE product_id  = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;

	}

	@Override
	public int update(Product object) {
		Connection conn = null;
		Database db = Database.getInstance();
		conn = db.getConnection();
		//process
		int row = 0;
		try {
			String sql = "UPDATE Product SET product_name = ?, product_price = ?,product_img = ? WHERE product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, object.getName());
			stmt.setInt(2, object.getPrice());
			stmt.setString(3, object.getImg());
			stmt.setInt(4, object.getId());

			row = stmt.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		db.close();
		return row;
	}

}
