/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Dao.CustomerDao;
import Dao.ProductDao;
import Dao.ReceiptDao;
import Dao.UserDao;
import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.User;
import java.util.Date;

/**
 *
 * @author norma
 */
public class TestReceipt {
    public static void main(String[] args) {
        CustomerDao cusDao = new CustomerDao();
        UserDao userDao = new UserDao();
        ReceiptDao rcDao = new ReceiptDao();
        ProductDao prodDao = new ProductDao();
        
        User user = userDao.get(1);
        Customer cus = cusDao.get(1);
        Product prod1 = prodDao.get(1);
        Product prod2 = prodDao.get(2);
        Date date = new Date();
        Receipt receipt = new Receipt(date,user,cus);
        receipt.addReceiptDetail(prod1, 2);
        receipt.addReceiptDetail(prod2, 1);
//        System.out.println(receipt);
//        rcDao.add(receipt);
//        System.out.println(rcDao.getAll());
         System.out.println(rcDao.get(1));
        
    }
}
